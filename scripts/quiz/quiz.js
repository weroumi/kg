
const surveyJson = {
    pages: [{
        elements: [{
            type: "radiogroup",
            name: "civilwar",
            title: "Вкажіть приклад алгебраїчного фракталу",
            choices: [
                "Трикутник Серпінського", "Множина Жуліа", "Крива Коха"
            ],
            correctAnswer: "Множина Жуліа"
        }]
    }, {
        elements: [{
            type: "radiogroup",
            name: "libertyordeath",
            title: "Які напрямки досліджень пов'язані з фракталами в сучасній науці та математиці?",
            choicesOrder: "random",
            choices: [
                "Лише теоретичні аспекти", "Медичні дослідження, криптографія, фізика хаосу", "Тільки галузь комп'ютерних наук"
            ],
            correctAnswer: "Медичні дослідження, криптографія, фізика хаосу"
        }]
    }, {
        elements: [{
            type: "radiogroup",
            name: "magnacarta1",
            title: "Які величини визначають відтінок в колірній моделі HSL?",
            choicesOrder: "random",
            choices: [
                "Кут від 0° до 360°",
                "Відсоток від 0% до 100%",
                "Значення від 0 до 255"
            ],
            correctAnswer: "Кут від 0° до 360°"
        }]
    }, {
        elements: [{
            type: "radiogroup",
            name: "magnacarta2",
            title: "Які переваги має колірна модель HSL порівняно з RGB?",
            choicesOrder: "random",
            choices: [
                "Легше змінювати яскравість",
                "Краща сумісність з пристроями відтворення кольорів"
            ],
            correctAnswer: "Легше змінювати яскравість"
        }]
    }, {
        elements: [{
            type: "radiogroup",
            name: "magnacarta3",
            title: "Як виникає білий колір у колірній моделі RGB?",
            choicesOrder: "random",
            choices: [
                "Сума максимальних значень у всіх компонентах",
                "Змішування всіх кольорів у спеціальному порядку",
                "Відсутність значень у всіх компонентах"
            ],
            correctAnswer: "Сума максимальних значень у всіх компонентах"
        }]
    }, {
        elements: [{
            type: "radiogroup",
            name: "magnacarta4",
            title: "Який колір складається з максимальних значень червоного та зеленого, але нульового значення синього?",
            choicesOrder: "random",
            choices: [
                "Помаранчевий",
                "Жовтий",
                "Коричневий"
            ],
            correctAnswer: "Жовтий"
        }]
    }],
    completedHtml: "<h4>Отримано <b>{correctAnswers}</b>/<b>{questionCount}</b> правильних відповідей.</h4>",
    completedHtmlOnCondition: [{
        expression: "{correctAnswers} == 0",
        html: "<h4>На жаль, жодна з відповідей не є праильною.</h4>"
    }, {
        expression: "{correctAnswers} == {questionCount}",
        html: "<h4>Вітання! Ви відповіли на всі питання правильно!</h4>"
    }]
};

const survey = new Survey.Model(surveyJson);
survey.applyTheme({
    "backgroundImage": "",
    "backgroundImageFit": "cover",
    "backgroundImageAttachment": "scroll",
    "backgroundOpacity": 1,
    "isPanelless": true,
    "cssVariables": {
        "--sjs-general-backcolor": "rgba(39, 40, 50, 1)",
        "--sjs-general-backcolor-dark": "rgba(46, 47, 58, 1)",
        "--sjs-general-backcolor-dim": "#351974",
        "--sjs-general-backcolor-dim-light": "rgba(32, 33, 43, 1)",
        "--sjs-general-backcolor-dim-dark": "rgba(37, 38, 48, 1)",
        "--sjs-general-forecolor": "rgba(213, 215, 238, 1)",
        "--sjs-general-forecolor-light": "rgba(117, 120, 140, 1)",
        "--sjs-general-dim-forecolor": "rgba(213, 215, 238, 1)",
        "--sjs-general-dim-forecolor-light": "rgba(117, 119, 141, 1)",
        "--sjs-primary-backcolor": "#FF3A99",
        "--sjs-primary-backcolor-light": "rgba(255, 58, 153, 0.1)",
        "--sjs-primary-backcolor-dark": "rgba(263, 60, 158, 1)",
        "--sjs-primary-forecolor": "rgba(32, 32, 32, 1)",
        "--sjs-primary-forecolor-light": "rgba(32, 32, 32, 0.25)",
        "--sjs-base-unit": "8px",
        "--sjs-corner-radius": "4px",
        "--sjs-secondary-backcolor": "rgba(255, 152, 20, 1)",
        "--sjs-secondary-backcolor-light": "rgba(255, 152, 20, 0.1)",
        "--sjs-secondary-backcolor-semi-light": "rgba(255, 152, 20, 0.25)",
        "--sjs-secondary-forecolor": "rgba(48, 48, 48, 1)",
        "--sjs-secondary-forecolor-light": "rgba(48, 48, 48, 0.25)",
        "--sjs-shadow-small": "0px 0px 0px 1px rgba(255, 255, 255, 0.1),0px 8px 16px 0px rgba(0, 0, 0, 0.15),0px 2px 4px 0px rgba(0, 0, 0, 0.2)",
        "--sjs-shadow-medium": "inset 0px 0px 0px 1px rgba(255, 255, 255, 0.05),0px 2px 6px 0px rgba(0, 0, 0, 0.2)",
        "--sjs-shadow-large": "0px 8px 16px 0px rgba(0, 0, 0, 0.2)",
        "--sjs-shadow-inner": "0px 0px 0px 1px rgba(255, 255, 255, 0.03),inset 0px 1px 4px 0px rgba(0, 0, 0, 0.2)",
        "--sjs-border-light": "rgba(54, 56, 69, 1)",
        "--sjs-border-default": "rgba(67, 69, 85, 1)",
        "--sjs-border-inside": "rgba(255, 255, 255, 0.08)",
        "--sjs-special-red": "rgba(254, 76, 108, 1)",
        "--sjs-special-red-light": "rgba(254, 76, 108, 0.1)",
        "--sjs-special-red-forecolor": "rgba(48, 48, 48, 1)",
        "--sjs-special-green": "rgba(36, 197, 164, 1)",
        "--sjs-special-green-light": "rgba(36, 197, 164, 0.1)",
        "--sjs-special-green-forecolor": "rgba(48, 48, 48, 1)",
        "--sjs-special-blue": "rgba(91, 151, 242, 1)",
        "--sjs-special-blue-light": "rgba(91, 151, 242, 0.1)",
        "--sjs-special-blue-forecolor": "rgba(48, 48, 48, 1)",
        "--sjs-special-yellow": "rgba(255, 152, 20, 1)",
        "--sjs-special-yellow-light": "rgba(255, 152, 20, 0.1)",
        "--sjs-special-yellow-forecolor": "rgba(48, 48, 48, 1)",
        "--sjs-article-font-xx-large-textDecoration": "none",
        "--sjs-article-font-xx-large-fontWeight": "700",
        "--sjs-article-font-xx-large-fontStyle": "normal",
        "--sjs-article-font-xx-large-fontStretch": "normal",
        "--sjs-article-font-xx-large-letterSpacing": "0",
        "--sjs-article-font-xx-large-lineHeight": "64px",
        "--sjs-article-font-xx-large-paragraphIndent": "0px",
        "--sjs-article-font-xx-large-textCase": "none",
        "--sjs-article-font-x-large-textDecoration": "none",
        "--sjs-article-font-x-large-fontWeight": "700",
        "--sjs-article-font-x-large-fontStyle": "normal",
        "--sjs-article-font-x-large-fontStretch": "normal",
        "--sjs-article-font-x-large-letterSpacing": "0",
        "--sjs-article-font-x-large-lineHeight": "56px",
        "--sjs-article-font-x-large-paragraphIndent": "0px",
        "--sjs-article-font-x-large-textCase": "none",
        "--sjs-article-font-large-textDecoration": "none",
        "--sjs-article-font-large-fontWeight": "700",
        "--sjs-article-font-large-fontStyle": "normal",
        "--sjs-article-font-large-fontStretch": "normal",
        "--sjs-article-font-large-letterSpacing": "0",
        "--sjs-article-font-large-lineHeight": "40px",
        "--sjs-article-font-large-paragraphIndent": "0px",
        "--sjs-article-font-large-textCase": "none",
        "--sjs-article-font-medium-textDecoration": "none",
        "--sjs-article-font-medium-fontWeight": "700",
        "--sjs-article-font-medium-fontStyle": "normal",
        "--sjs-article-font-medium-fontStretch": "normal",
        "--sjs-article-font-medium-letterSpacing": "0",
        "--sjs-article-font-medium-lineHeight": "32px",
        "--sjs-article-font-medium-paragraphIndent": "0px",
        "--sjs-article-font-medium-textCase": "none",
        "--sjs-article-font-default-textDecoration": "none",
        "--sjs-article-font-default-fontWeight": "400",
        "--sjs-article-font-default-fontStyle": "normal",
        "--sjs-article-font-default-fontStretch": "normal",
        "--sjs-article-font-default-letterSpacing": "0",
        "--sjs-article-font-default-lineHeight": "28px",
        "--sjs-article-font-default-paragraphIndent": "0px",
        "--sjs-article-font-default-textCase": "none",
        "--sjs-header-backcolor": "#351974"
    },
    "themeName": "layered_exported",
    "colorPalette": "dark",
    "header": {
        "height": 256,
        "inheritWidthFrom": "container",
        "textAreaWidth": 512,
        "overlapEnabled": false,
        "backgroundImageOpacity": 1,
        "backgroundImageFit": "cover",
        "logoPositionX": "right",
        "logoPositionY": "top",
        "titlePositionX": "left",
        "titlePositionY": "bottom",
        "descriptionPositionX": "left",
        "descriptionPositionY": "bottom"
    }
});
$(function() {
    $("#surveyContainer").Survey({ model: survey });
});