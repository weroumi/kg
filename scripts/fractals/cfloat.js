function cfloat(re, im){
    this.re = re;
    this.im = im;
  
    this.copy = function(){
      return new cfloat(this.re, this.im)
    }
    this.sub = function(z) {
      let new_re = this.re - z.re;
      let new_im = this.im - z.im;
      return new cfloat(new_re, new_im);
    }
    this.add = function(z) {
      let new_re = this.re + z.re;
      let new_im = this.im + z.im;
      return new cfloat(new_re, new_im);
    }
    this.mult = function(z) {
      let new_re = this.re * z.re - this.im * z.im;
      let new_im = this.re * z.im + this.im * z.re;
      return new cfloat(new_re, new_im);
    }
    this.div = function(z) {
      let re_num = this.re * z.re + this.im * z.im;
      let im_num = this.im * z.re - this.re * z.im;
      let denom = z.re * z.re + z.im * z.im;
      return new cfloat(re_num / denom, im_num / denom);
    }
    this.magSq = function() {
      return this.re * this.re + this.im * this.im;
    }
    this.scale = function(x) {
      return new cfloat(this.re * x, this.im * x);
    }
    this.distSq = function(z) {
      let dre = this.re - z.re;
      let dim = this.im - z.im;
      return dre * dre + dim * dim;
    }
    this.pow = function(n) {
      let z = this.copy();
      let m = n;
      if (n > 1) {
        while (m > 1) {
          z = this.mult(z);
          m--;
        }
      } else if (n < 1) {
        while (m < 1) {
          z = this.div(z);
          m++;
        }
      }
      return z;
    }
  }