document.getElementById("ice-btn").addEventListener('click', ()=>{
  window.location.replace("icefractal.html")
})
document.getElementById("header-btn").addEventListener('click', ()=>{
  window.location.replace("../main/educationFractals.html")
})
document.getElementById("back-btn").addEventListener('click', ()=>{
  window.location.replace("../../index.html")
})
let center = new cfloat(0, 0);
let radius = 1.5;
//ROOTSSSS------------------------
let roots = [];
let re = 1
let im = 0
roots.push(new cfloat(re, im))
re = 0.309016994374947
im = 0.951056516295154
roots.push(new cfloat(re, im))
roots.push(new cfloat(re, -1 * im))
re = -1 * 0.809016994374947
im = 0.587785252292473
roots.push(new cfloat(re, im))
roots.push(new cfloat(re, -1 * im))
//COLORSSSSS-------------------------------
let root_colors = [];
//-----------------------------------------
let tolerance = 0.1;
let c;

async function setup() {
  cnvDiv =  document.getElementById('img')
  // cnv = createCanvas(cnvDiv.offsetWidth, cnvDiv.offsetHeight);
  cnv = createCanvas(350, 350)
  cnv.parent('img')
  c = 1
  cnv.mousePressed(mousePress);
  pixelDensity(1);
  document.getElementById("loading").classList.remove("invisible")
  document.getElementById("loading").classList.add("visible")
  
}

function draw(){
  background("#4b27a0");
  key_color = document.getElementById("key-color").value
  console.log("kk" + key_color)
  createColorPalette(key_color);
}

function complexToPixel(z) {
  let px = map(z.re, center.re - radius, center.re + radius, 0, width);
  let py = map(z.im, center.im - radius, center.im + radius, height, 0);
  return createVector(px, py);
}

function pixelToComplex(vec) {
  let re = map(vec.x, 0, width, center.re - radius, center.re + radius);
  let im = map(vec.y, height, 0, center.im - radius, center.im + radius);
  return new cfloat(re, im);
}

function drawRoots() {
  strokeWeight(6);
  for (let i = 0; i < roots.length; i++) {
    console.log(root_colors[i]);
    stroke(root_colors[i]);
    point(complexToPixel(roots[i]));
  }
  return true
}

function f(z) {
  let vec = new cfloat(1, 0);
  // let num = new cfloat(1, 0)
  for (let i = 0; i < roots.length; i++) {
    let factor = z.sub(roots[i]);
    // let factor = z;
    vec = vec.mult(factor);
  }
  // vec = vec.add(num)
  return vec;
}

function df(z) {
  let vec = new cfloat(0, 0);
  for (let j = 0; j < roots.length; j++) {
    let factor = new cfloat(1, 0);
    for (let i = 0; i < roots.length; i++) {
      if (i != j) {
        factor = factor.mult(z.sub(roots[i]));
      }
    }
    vec = vec.add(factor);
  }
  return vec;
}

function newtonsMethodN(z, alfa) {
  let n = 0;
  let x = z.copy();
  let sq_dist = 1000;
  while (n < 100 && sq_dist > tolerance * tolerance) {
    let prev = x.copy();
    let fx = f(x);
    let dfx = df(x);
    while (dfx.magSq() == 0) {
      let v = new cfloat(random(0, 0.01), random(0, 0.01));
      dfx = dfx.add(v);
    }
    x = x.sub(fx.div(dfx).scale(alfa));
    sq_dist = x.distSq(prev);
    n++;
  }
  return x;
}

function createFractal1(alfa) {
  loadPixels();
  for (let j = 0; j < height; j++) {
    for (let i = 0; i < width; i++) {
      let pix = (i + j * width) * 4;
      let v = pixelToComplex(createVector(i, j));
      v = newtonsMethodN(v, c);
      let got_to_root = false;
      for (let k = 0; k < roots.length; k++) {
        let dsq = roots[k].distSq(v);
        if (dsq < tolerance * tolerance) {
          got_to_root = true;
          pixels[pix + 0] = red(root_colors[k]);
          pixels[pix + 1] = green(root_colors[k]);
          pixels[pix + 2] = blue(root_colors[k]);
          pixels[pix + 3] = 255;
          break;
        }
      }
      if (!got_to_root) {
        pixels[pix + 0] = 0;
        pixels[pix + 1] = 0;
        pixels[pix + 2] = 0;
        pixels[pix + 3] = 255;
      }
    }
  }
  updatePixels();
  noLoop()
}

function mousePress() {
  let v = pixelToComplex(createVector(mouseX, mouseY));
    if (mouseButton == LEFT) {
      center = v;
      radius /= 2;
    } else if (mouseButton == RIGHT) {
      center = v;
      radius *= 2;
    }
    redraw();
}

document.getElementById("apply").addEventListener('click', ()=>{
  root_colors = []
  key_color = document.getElementById("key-color").value
  console.log("Key color reset: " + key_color)
  c = document.getElementById('c').value
  redraw()
})

async function createColorPalette(hex){
  document.getElementById("loading").classList.remove("invisible")
  document.getElementById("loading").classList.add("visible")
  let mode = document.getElementById("mode").value
  fetch(`https://www.thecolorapi.com/scheme?hex=${hex.replace("#", "")}&mode=${mode}&count=5`).then(response =>{
    if (!response.ok) {
        throw new Error("resp not ok")
    }
    return response.json()
  }).then(json => {
    json.colors.forEach(c =>{
      root_colors.push(c.hex.value)
    })
    if(root_colors.length != 0){
      drawRoots();
      createFractal1(1);
      document.getElementById("loading").classList.remove("visible")
      document.getElementById("loading").classList.add("invisible")
    }
  })
  noLoop()
}

document.getElementById('zoom-in').addEventListener('click', () => {
  center = new cfloat(0, 0);
  radius /= 2;
  redraw()
})
document.getElementById('zoom-out').addEventListener('click', () => {
  center = new cfloat(0, 0);
  radius *= 2;
  redraw()
  })

document.getElementById('ice-btn').addEventListener('click', ()=>{
  window.location.replace("icefractal.html")
})