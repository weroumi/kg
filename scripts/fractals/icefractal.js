document.getElementById("newton-btn").addEventListener('click', ()=>{
    window.location.replace("ntnfractal.html")
})
document.getElementById("header-btn").addEventListener('click', ()=>{
    window.location.replace("../main/educationFractals.html")
})
document.getElementById("back-btn").addEventListener('click', ()=>{
    window.location.replace("../../index.html")
})
const canvas = document.getElementById("fractalCanvas");
const ctx = canvas.getContext("2d");

function drawLine(x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
}

function draw(x, y, l, u, t) {
    if (t > 0) {
        l *= 0.5;
        [x, y] = draw2(x, y, l, u, t-1);
        [x, y] = draw2(x, y, l*0.8, u + Math.PI / 2, t-1);
        [x, y] = draw2(x, y, l*0.8, u - Math.PI / 2, t-1);
        [x, y] = draw2(x, y, l, u, t-1);
    } else {
        drawLine(x, y, x + Math.cos(u) * l, y - Math.sin(u) * l);
    }
}

function draw2(x, y, l, u, t) {
    draw(x, y, l, u, t);
    return [x + l * Math.cos(u), y - l * Math.sin(u)];
}
function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

document.getElementById("apply").addEventListener("click", function () {
    clearCanvas();
    const n = document.getElementById('c').value;
    draw(410, 10, 400, -Math.PI, n);
    draw(10, 410, 400, 0, n);
    draw(10, 10, 400, -Math.PI / 2, n);
    draw(410, 410, 400, Math.PI / 2, n);
});

document.getElementById('newton-btn').addEventListener('click', ()=>{
    window.location.replace("ntnfractal.html")
  })
