document.getElementById("header-btn").addEventListener('click', ()=>{
    window.location.replace("../main/educationAffine.html")
})
document.getElementById("back-btn").addEventListener('click', ()=>{
    window.location.replace("../../index.html")
})
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext('2d');
const container = document.getElementById("right");

const x = document.getElementById('x');
const y = document.getElementById('y');
const radius = document.getElementById('radius');
const angleDegree = document.getElementById('angle');
const scale = document.getElementById('k');
let select = document.getElementById('select');
let scaleValue = document.getElementById('k-value')
let isCreate = false;

let hexagonVertices = [];
let hexagonRotateVertices = []

let cameraOffset = { x: 0, y: 0 }
let cameraZoom = 1;
const MAX_ZOOM = 20;
const PIXELS_PER_UNIT = 10;
const ANIMATION_TIME = 2000;
let startTime = 0;


function resizeCanvas() {
    canvas.width = container.clientWidth;
    canvas.height = container.clientHeight;
    drawHexagonByCenter();
}

function createSelect(){
    hexagonRotateVertices = []
    hexagonVertices.forEach(v => {
        hexagonRotateVertices.push(math.divide(v, 10))
    })
    hexagonRotateVertices.push([parseFloat(x.value), parseFloat(y.value)])
    if(hexagonVertices.length == 6){
        select.innerHTML = ""
        var op = document.createElement('option');
        op.value = 6;
        op.innerHTML = "центр";
        select.appendChild(op);
        for (var i = 0; i < 6; i++){
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = `(${math.round(hexagonVertices[i][0] / 10, 0)}; ${-math.round(hexagonVertices[i][1] / 10, 0)})`;
            select.appendChild(opt);
        }
    }
}


 function drawHexagonByCoordinates()
 {
    const { width, height } = canvas;
    const centerX = canvas.width / 2;
    const centerY = canvas.height / 2;

    ctx.reset()
    ctx.fillStyle = '#542f94';
    console.log('width:', width, 'height:', height);
    ctx.fillRect(0, 0, width, height);    
    
    ctx.translate(width / 2 + cameraOffset.x, height / 2 + cameraOffset.y);
    ctx.scale(cameraZoom, cameraZoom);
    if (hexagonVertices.length < 6) {
        console.error(hexagonVertices.length);
        return;
    }
    
    drawCoordinates();
    
    ctx.fillStyle = '#FF3A99';
    ctx.beginPath();

    ctx.moveTo(parseInt(hexagonVertices[0][0]), parseInt(hexagonVertices[0][1]));
    for (let i = 1; i < 6; i++) {
        const X = hexagonVertices[i][0];
        const Y = hexagonVertices[i][1];
        ctx.lineTo(X, Y);
    }
    ctx.closePath();
    ctx.fill();
 }

 function drawHexagonByCenter()
 {
    ctx.reset()
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    hexagonVertices = [];
    const { width, height } = canvas;
    ctx.fillStyle = '#542f94';
    ctx.fillRect(0, 0, width, height);
    ctx.save();
    ctx.translate(width / 2 + cameraOffset.x, height / 2 + cameraOffset.y);
    ctx.scale(cameraZoom, cameraZoom);
    drawCoordinates();
    ctx.fillStyle = '#FF3A99';
    ctx.beginPath();
    const centerX = x.value * PIXELS_PER_UNIT;
    const centerY = y.value * PIXELS_PER_UNIT;
    ctx.moveTo(centerX + radius.value*PIXELS_PER_UNIT, centerY);
    for (let i = 1; i <= 6; i++) {
        const angle = (i * Math.PI) / 3; 
        const X = centerX + radius.value*PIXELS_PER_UNIT * Math.cos(angle);
        const Y = centerY + radius.value*PIXELS_PER_UNIT * Math.sin(angle);
        ctx.lineTo(X, Y);
        hexagonVertices.push([X,Y,1]);
    }
   ctx.closePath();
   ctx.fill();
   ctx.restore();
    isCreate=true;
    createSelect()
 }


function drawCoordinates() {
    const { width: preZoomWidth, height: preZoomHeight } = canvas;

    const width = preZoomWidth / cameraZoom;
    const height = preZoomHeight / cameraZoom;

    const xLines = 500;
    const yLines = 500;

    for (let i = 0; i <= xLines; i++) {
        ctx.beginPath();
        ctx.lineWidth = 1;

        ctx.strokeStyle = "#6D6584";
        ctx.moveTo(-cameraOffset.x / cameraZoom - width / 2, PIXELS_PER_UNIT * (i - xLines / 2));
        ctx.lineTo(-cameraOffset.x / cameraZoom + width / 2, PIXELS_PER_UNIT * (i - xLines / 2));
        ctx.stroke();
    }

    for (let i = 0; i <= yLines; i++) {
        ctx.beginPath();
        ctx.lineWidth = 1;

        ctx.strokeStyle = "#6D6584";
        ctx.moveTo(PIXELS_PER_UNIT * (i - yLines / 2), -cameraOffset.y / cameraZoom - height / 2);
        ctx.lineTo(PIXELS_PER_UNIT * (i - yLines / 2), -cameraOffset.y / cameraZoom + height / 2);
        ctx.stroke();
    }

    ctx.strokeStyle = "#bbb";
    ctx.moveTo(0, -cameraOffset.y / cameraZoom - height / 2);
    ctx.lineTo(0, -cameraOffset.y / cameraZoom + height / 2);
    ctx.stroke();

    ctx.moveTo(-cameraOffset.x / cameraZoom - width / 2, 0);
    ctx.lineTo(-cameraOffset.x / cameraZoom + width / 2, 0);
    ctx.stroke();

    ctx.fillStyle = '#ccc';
    for (let i = -xLines / 2; i < xLines / 2; i++) {
        if (i % 5 === 0) {
            ctx.font = '10px Arial';
            ctx.textAlign = 'center';
            ctx.fillText(i, i * PIXELS_PER_UNIT, 10);
        }
    }

    for (let i = -yLines / 2; i < yLines / 2; i++) {
        if (i % 5 === 0 && i !== 0) {
            ctx.font = '10px Arial';
            ctx.textAlign = 'end';
            ctx.fillText(-i, -2, i * PIXELS_PER_UNIT);
        }
    }
}

function rotateAroundPoint(Ox,Oy, repetitions) {
    var i = 0;
    var intervalID = window.setInterval(function () {
        let angle = (angleDegree.value * Math.PI) / 180;
        angle /= repetitions
        let scaleValue = (scale.value / 50 + 23) / repetitions
        let X = Ox*PIXELS_PER_UNIT;
        let Y = Oy*PIXELS_PER_UNIT;
        let rotateMatrix = math.matrix([[Math.cos(angle),Math.sin(angle),0],[-Math.sin(angle), Math.cos(angle),0],[0,0,1]])
        let minusTmatrix = math.matrix([[1,0,0],[0,1,0],[-X,-Y,1]]);
        let Tmatrix =  math.matrix([[1,0,0],[0,1,0],[X,Y,1]]);
        let scaleMatrix = math.matrix([[scaleValue,0,0],[0,scaleValue,0],[0,0,1]]);
        let  transformationMatrix = math.multiply(minusTmatrix,rotateMatrix);
        transformationMatrix = math.multiply(transformationMatrix,scaleMatrix);
        transformationMatrix=math.multiply(transformationMatrix, Tmatrix);
        let resultMatrix = math.multiply(hexagonVertices,transformationMatrix);
        hexagonVertices=resultMatrix.toArray();  
        createSelect()
        drawHexagonByCoordinates();
        if (i++ === repetitions) {
            window.clearInterval(intervalID);
        }
     }, 50)
}

let download = () => {
  drawHexagonByCenter()
  const link = document.createElement('a');
  link.download = 'download.png';
  link.href = canvas.toDataURL();
  link.click();
  link.delete;
}


window.addEventListener('resize', resizeCanvas)
document.getElementById("create").addEventListener("click",drawHexagonByCenter)
document.getElementById("show").addEventListener("click",() => rotateAroundPoint(hexagonRotateVertices[select.value][0], 
    hexagonRotateVertices[select.value][1], 23))
document.getElementById("save").addEventListener("click", download);
scale.addEventListener('input', () => {
    scaleValue.innerHTML = scale.value
    console.log(scale.value)
})
resizeCanvas();