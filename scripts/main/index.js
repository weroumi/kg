document.getElementById("fractals").addEventListener('click', ()=>{
    window.location.replace("/pages/fractals/icefractal.html")
})
document.getElementById("colorModels").addEventListener('click', ()=>{
    window.location.replace("/pages/colorModels/colorModels.html")
})
document.getElementById("affine").addEventListener('click', ()=>{
    window.location.replace("/pages/affineTransformation/affine.html")
})
document.getElementById("header-btn").addEventListener('click', ()=>{
    window.location.replace("/pages/main/documentation.html")
})