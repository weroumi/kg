document.getElementById("header-btn").addEventListener('click', ()=>{
    window.location.replace("../main/educationColors.html")
})
document.getElementById("back-btn").addEventListener('click', ()=>{
    window.location.replace("../../index.html")
  })
let slider = document.getElementById("luminance-range")
let sliderValue = document.getElementById("luminance-value")
sliderValue.innerHTML = slider.value
const incanvas = document.getElementById('input-canvas')
const outcanvas = document.getElementById('output-canvas')
const selectcanvas = document.getElementById('c2');
const inctx = incanvas.getContext('2d')
const outctx = outcanvas.getContext('2d')
export let imageDataX = 0
export let imageDataY = 0
export let imageDataWidth = 0
export let imageDataHeight = 0
export function setImageDataX(f) {
    imageDataX = f
}
export function setImageDataY(f) {
    imageDataY = f
}
export function setImageDataWidth(f) {
    imageDataWidth = f
}
export function setImageDataHeight(f) {
    imageDataHeight = f
}
let img

slider.addEventListener('input', () => {
    sliderValue.innerHTML = slider.value
    const selectedValue = parseInt(document.getElementById('color-list').value)
    let range = [selectedValue,selectedValue+60]
    changeBrightnessInRange(range)
})

document.getElementById('upload-image').addEventListener('input', event => {
    const file = event.target.files[0]
    if (file) {
        const image = new Image()
        image.onload = () => {
           resizeCanvas()      
        }
        image.src = URL.createObjectURL(file)
        img = image
        document.querySelector("#image").classList.add("non-visible")
        document.querySelector(".values").classList.remove("non-visible")
        document.querySelector(".before-after").classList.remove("non-visible")
    }
})

function getMousePosition(canvas, evt) {
    let rect = canvas.getBoundingClientRect()
    return {
        x: Math.round(evt.clientX - rect.left),
        y: Math.round(evt.clientY - rect.top)
    }
}

window.addEventListener('resize', resizeCanvas)

function resizeCanvas() {
    incanvas.width = incanvas.scrollWidth
    incanvas.height=incanvas.scrollHeight
    outcanvas.width=outcanvas.scrollWidth
    outcanvas.height=outcanvas.scrollHeight
    const canvasWidth = incanvas.width
    incanvas.height=outcanvas.height=9*incanvas.width/16
   
    const canvasHeight = incanvas.height
    const outWidth = outcanvas.width
    inctx.drawImage(img, 0, 0, canvasWidth, canvasHeight)
    outctx.drawImage(img,0, 0, outWidth, canvasHeight)
    selectcanvas.width = outWidth
    selectcanvas.height = canvasHeight 
}
outcanvas.addEventListener('mousemove', event => {
    const pos = getMousePosition(outcanvas, event)
    const pixel = getRgbPixel(outcanvas, pos.x, pos.y)
    updateColors(pixel.r, pixel.g, pixel.b)
})

selectcanvas.addEventListener('mousemove', event => {
    const pos = getMousePosition(c2, event)
    const pixel = getRgbPixel(outcanvas, pos.x, pos.y)
    updateColors(pixel.r, pixel.g, pixel.b)
})

incanvas.addEventListener('mousemove', event => {
    const pos = getMousePosition(incanvas, event)
    const pixel = getRgbPixel(incanvas, pos.x, pos.y)
    updateColors(pixel.r, pixel.g, pixel.b)
})

outcanvas.addEventListener('mouseout', clearColors)
incanvas.addEventListener('mouseout', clearColors)

function updateColors(r, g, b) {
    document.getElementById('rgb').innerText = `(${r}, ${g}, ${b})`
    const color = `rgb(${r}, ${g}, ${b})`
    document.getElementById('rgb-div').style.border="5px solid " + color
    const {h, s, l} = rgb2hsl(r, g, b)
    document.getElementById('hsl').innerText = `(${h}°, ${s.toFixed(3)}, ${l.toFixed(3)})`
    document.getElementById('hsl-div').style.border="5px solid " + color
}

function clearColors() {
    document.getElementById('rgb').innerText = ''
    document.getElementById('hsl').innerText = ''
}

function rgb2hsl(r, g, b) {
    r /= 255
    g /= 255
    b /= 255

    let max = Math.max(r, g, b),
    min = Math.min(r, g, b);
    let h,
    s,
    l = (max + min) / 2

    let d = max - min
    s = max === 0 ? 0 : d / max

    if (max === min) {
        h = 0
    } else {
        switch (max) {
            case r:     h = (g - b) / d + (g < b ? 6 : 0)
                        break

            case g:     h = (b - r) / d + 2
                        break

            case b:     h = (r - g) / d + 4
                        break

            default:    break
        }
        h /= 6
    }
    h=Math.round(h*360)
    return {
        h: h,
        s: s,
        l: l
    }
}

function getRgbPixel(canvas, mouseX, mouseY) {
    let imageData = canvas.getContext('2d').getImageData(0, 0, canvas.scrollWidth, canvas.scrollHeight).data
    let basePos = mouseY * canvas.scrollWidth + mouseX
    basePos *= 4
    return {
        r: imageData[basePos],
        g: imageData[basePos + 1],
        b: imageData[basePos + 2]
    }
}

function changeBrightnessInRange(hueRange) {
    inctx.drawImage(img, 0, 0, outcanvas.width, outcanvas.height)
    if(imageDataWidth === 0){
        imageDataWidth = incanvas.width
    }
    if(imageDataHeight === 0){
        imageDataHeight = incanvas.height
    }
    const imageData = inctx.getImageData(imageDataX, imageDataY, imageDataWidth, imageDataHeight)
    for (let i = 0; i < imageData.data.length; i += 4) {
        const r = imageData.data[i]
        const g = imageData.data[i + 1]
        const b = imageData.data[i + 2]
        let {h, s, l} = rgb2hsl( r, g, b );
        if (h >= hueRange[0] && h <= hueRange[1]) {
            l = slider.value/100
            let {r,g,b} = hslToRgb(h,s,l)
            imageData.data[i] = r
            imageData.data[i + 1] = g
            imageData.data[i + 2] = b
        }
    }
    outctx.putImageData(imageData, imageDataX, imageDataY)

}

document.getElementById('color-list').addEventListener('change', (event) => {
    const selectedValue = parseInt(event.target.value)
    let range = [selectedValue,selectedValue+60]
    changeBrightnessInRange(range)  
})

function hslToRgb(h, s, l) {
    let r, g, b
    h/=360
    if (s === 0) {
        r = g = b = l 
    } else {
        const q = l < 0.5 ? l * (1 + s) : l + s - l * s
        const p = 2 * l - q
        r = hueToRgb(p, q, h + 1/3)
        g = hueToRgb(p, q, h)
        b = hueToRgb(p, q, h - 1/3)
    }
    return {
        r: Math.round(r * 255),
        g: Math.round(g * 255),
        b: Math.round(b * 255)
    }
}
  
function hueToRgb(p, q, t) {
    if (t < 0) t += 1
    if (t > 1) t -= 1
    if (t < 1/6) return p + (q - p) * 6 * t
    if (t < 1/2) return q
    if (t < 2/3) return p + (q - p) * (2/3 - t) * 6
    return p
}

document.getElementById("btn-reset").addEventListener('click', () =>{
    imageDataX = 0
    imageDataY = 0
    imageDataWidth = 0
    imageDataHeight = 0
    outctx.drawImage(incanvas, 0, 0)
    const c2ctx = c2.getContext('2d')
    c2ctx.clearRect(0, 0, c2.width, c2.height)
})



