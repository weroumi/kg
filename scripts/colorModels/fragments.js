let c2 = document.getElementById("c2")
let ctx2 = c2.getContext("2d")
let origin = null
import {setImageDataX, setImageDataY, setImageDataWidth, setImageDataHeight} from "./colorModels.js"

c2.onmousedown = e => { origin = {x: e.offsetX, y: e.offsetY} }

window.onmouseup = e => { origin = null }

c2.onmousemove = e => { 
    if (!!origin) { 
        ctx2.strokeStyle = "#00FF00"
        ctx2.setLineDash([1, 1])
        ctx2.clearRect(0, 0, c2.width, c2.height)
        ctx2.beginPath()
        let x = origin.x
        let y = origin.y
        setImageDataX(1 * x)
        setImageDataY(1 * y)
        setImageDataWidth(e.offsetX - x)
        setImageDataHeight(e.offsetY - y)
        ctx2.rect(x, y, e.offsetX - x, e.offsetY - y)
        ctx2.stroke()
    } 
}